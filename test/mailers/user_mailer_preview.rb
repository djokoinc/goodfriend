class UserMailerPreview < ApplicationMailerPreview

  def send_mail_to_beneficiares(expense, userTo)
    UserMailer.send_mail_to_beneficiares(expense,userTo)
  end
end
class ApplicationMailerPreview < ActionMailer::Preview
  default from: 'notif@goodfriend.com'
  layout 'mailer'

end

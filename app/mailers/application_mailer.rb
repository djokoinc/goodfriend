class ApplicationMailer < ActionMailer::Base
  default from: 'notif@goodfriend.com'
  layout 'mailer'

end

class UserMailer < ApplicationMailer

  def send_mail_to_beneficiares(expense, userTo)
    @user = userTo
    @expense = expense
    Expense.get_balance_of_user(userTo)
    mail(to: @user.email, subject: "Nouvelle dépense")
  end
end
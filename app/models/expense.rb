class Expense < ApplicationRecord
	belongs_to :user
	has_and_belongs_to_many :users
	after_create :send_mail_to_beneficiares_after_create

	monetize :amount_cents, :as => "amount_money"


	def amount
		self.amount_money.to_f
	end

	def payer
		self.user
	end

	def beneficiares
		self.users
	end

	def get_balance_of_user(user)
      @balance = 0      
      nUser = self.users.count
      part = self.amount_money / nUser
      if user.id == self.user.id 
        @balance = self.amount_money - part
      else 
        @balance = 0 - part 
      end
    end

     # Send a mials to inform beneficiares of expenses
    def send_mail_to_beneficiares_after_create 
      self.beneficiares.each do |user|
        UserMailer.send_mail_to_beneficiares(self,user).deliver_later
      end
    end

end
